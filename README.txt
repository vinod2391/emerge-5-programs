This contains the following programs:

1. Concatenate two strings
2. String length
3. String reverse
4. Palindrome
5. Change case
6. Remove char from a String